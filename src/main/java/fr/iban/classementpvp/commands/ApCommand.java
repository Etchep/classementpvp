package fr.iban.classementpvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.classementpvp.Main;
import fr.iban.classementpvp.sql.SQLCalls;

public class ApCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(sender instanceof Player) {
			final Player p = (Player)sender;
			if(args.length == 0) {
				p.sendMessage(new String[]{
						"§7/ap give <joueur> <nombre> §8Donner des points à un joueur.",
						"§7/ap reset <joueur> §8Reset les points d'un joueur",
						"§7/ap show <joueur> §8Voir les points d'un joueur",
						"§7/ap rankreset §8Reset le classement",
				});
			}else {
				switch (args[0]) {
				case "give":
					if(args.length == 3) {
						final Player target = Bukkit.getPlayer(args[1]);
						if(target != null) {
							int nombre;
							try {
								nombre = Integer.parseInt(args[2]);
							}
							catch (final NumberFormatException e)
							{
								nombre = 0;
							}
							setPoints(target, getPoints(target) + nombre);
							p.sendMessage("§aVous avez envoyé " + nombre + " points à " + target.getName() + ".");
						}else {
							p.sendMessage("§cLe joueur n'est pas en ligne !");
						}
					}else {
						p.sendMessage("§7/ap give <joueur> §8Donner des points à un joueur.");
					}
					break;
				case "reset":
					if(args.length == 2) {
						final Player target = Bukkit.getPlayer(args[1]);
						if(target != null) {
							setPoints(target, 0);
							p.sendMessage("§aLe joueur " + target.getName() + " a été reset.");
						}else {
							p.sendMessage("§cLe joueur n'est pas en ligne !");
						}
					}else {
						p.sendMessage("§7/ap reset <joueur> §8Reset les points d'un joueur");
					}
					break;
				case "show":
					if(args.length == 2) {
						final Player target = Bukkit.getPlayer(args[1]);
						if(target != null) {
							p.sendMessage("§aLe joueur " + target.getName() + " a §2" + getPoints(target) +" points§a.");
						}else {
							p.sendMessage("§cLe joueur n'est pas en ligne !");
						}
					}else {
						p.sendMessage("§7/ap reset <joueur> §8Reset les points d'un joueur");
					}
					break;
				case "rankreset":
					SQLCalls.resetClassement();
					p.sendMessage("§aReset du classement en cours...");
					break;
				default:
					p.chat("/ap");
					break;
				}
			}
		}
		return false;
	}

	private int getPoints(Player p) {
		return Main.getInstance().playersMap.get(p.getUniqueId());
	}

	private void setPoints(Player p, int amount) {
		Main.getInstance().playersMap.put(p.getUniqueId(), amount);
	}

}
