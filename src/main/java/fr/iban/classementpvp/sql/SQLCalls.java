package fr.iban.classementpvp.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.iban.classementpvp.Main;
import fr.iban.classementpvp.data.TopTenData;
import fr.iban.classementpvp.sql.runnables.QueryRunnable;
import fr.iban.classementpvp.sql.runnables.UpdateRunnable;

public class SQLCalls {

	private static final String CREATE_TABLE_SQL="CREATE TABLE IF NOT EXISTS Players ("
			+ "id INT AUTO_INCREMENT PRIMARY KEY,"
			+ "uuid VARCHAR(36) NOT NULL,"
			+ "points INT NOT NULL,"
			+ "oldpoints INT NOT NULL,"
			+ "UNIQUE KEY (uuid))";

	//ADD PLAYER TO DB
	public static void addPlayerToDatabase(Player player, Callback<Integer, SQLException> callback) {
		try {
			new UpdateRunnable(DbManager.BD.getDbAccess().getConnection(), "INSERT INTO `Players` (uuid, points, oldpoints) VALUES ('"+player.getUniqueId().toString()+"', 0, 0) ON DUPLICATE KEY UPDATE uuid='"+player.getUniqueId()+"'", callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}  

	//GET PLAYER POINTS
	public static void getPlayerPoints(Player player, Callback<ResultSet, SQLException> callback) {
		try {
			new QueryRunnable(DbManager.BD.getDbAccess().getConnection(), "SELECT points FROM `Players` WHERE uuid='"+player.getUniqueId().toString()+"'", callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//CHECKIFEXIST
	public static void existCheck(Player player, Callback<ResultSet, SQLException> callback) {
		try {
			new QueryRunnable(DbManager.BD.getDbAccess().getConnection(), "SELECT uuid FROM `Players` WHERE uuid='"+player.getUniqueId().toString()+"'", callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//UPDATE PLAYER POINTS
	public static void updatePlayerPoints(Player player, int points, Callback<Integer, SQLException> callback) {
		try {
			new UpdateRunnable(DbManager.BD.getDbAccess().getConnection(), "UPDATE `Players` SET points="+points+" WHERE uuid='"+player.getUniqueId().toString()+"'", callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//RESET PLAYER POINTS AND SAVE TO OLDPOINTS
	public static void resetPoints(String uuid, int points, Callback<Integer, SQLException> callback) {
		try {
			new UpdateRunnable(DbManager.BD.getDbAccess().getConnection(), "UPDATE `Players` SET points=0, oldpoints="+points+" WHERE uuid='"+uuid+"'", callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void selectPoints(Callback<ResultSet, SQLException> callback) {
		try {
			new QueryRunnable(DbManager.BD.getDbAccess().getConnection(), "SELECT uuid, points FROM `Players`", callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	//GET TOP 10
	public static void getTopTen(Callback<ResultSet, SQLException> callback) {
		try {
			new QueryRunnable(DbManager.BD.getDbAccess().getConnection(), "SELECT uuid, points FROM `Players` ORDER BY points DESC", callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	//CREATE TABLES
	public static void createTable(Callback<Integer, SQLException> callback) {
		try {
			new UpdateRunnable(DbManager.BD.getDbAccess().getConnection(), CREATE_TABLE_SQL , callback).runTaskAsynchronously(Main.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public static void resetClassement() {
		Bukkit.getOnlinePlayers().forEach((player) -> {
			if(Main.getInstance().playersMap.containsKey(player.getUniqueId())) {
				updatePlayerPoints(player, Main.getInstance().playersMap.get(player.getUniqueId()), (result, thrown) -> {});
				Main.getInstance().playersMap.put(player.getUniqueId(), 0);
			}
		});
		selectPoints((result, thrown) -> {
			if(thrown == null) {
				try {
					while(result.next()) {
						resetPoints(result.getString("uuid"), result.getInt("points"), (rs, t) -> {});
					}
				} catch (final SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				thrown.printStackTrace();
			}
		});
	}
	
	public static void createTables() {
		createTable((rs, t) -> {
			if(rs != null) {
				System.out.println("ClassementPvP : Création de la table Players(uuid,points) si elle n'éxistait pas.");
			}else {
				System.out.println("ClassementPvP : Erreur lors de la création de la table Players(uuid,points).");
				t.printStackTrace();
			}
		});
	}

	public static void updateTopTen() {
		getTopTen((result, thrown) -> {
			try {
				if(result != null) {
					int i = 0;
					while(result.next()) {
						i++;
						final UUID uuid = UUID.fromString(result.getString("uuid"));
						final int points = result.getInt("points");
						Main.getInstance().top10.put(i, new TopTenData(points, uuid));
					}
				}else {
					thrown.printStackTrace();
				}
			} catch (final SQLException e) {
				e.printStackTrace();
			}
		});
	}


	public static void updateAllPlayersPoints() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			if(Main.getInstance().playersMap.containsKey(player.getUniqueId())) {
				updatePlayerPoints(player, Main.getInstance().playersMap.get(player.getUniqueId()), (result, thrown) -> {});
			}
		});
	}

	public static void updatePoints(Player player, int points) {
		updatePlayerPoints(player, points, (result, thrown) -> {
			if(result != null) {
				System.out.println("ClassementPvP : Les données de " + player.getName() + " ont été sauvegardées sur la bdd.");
			}
		});
	}

	public static void addPlayerToMap(Player player) {
		addPlayerToDatabase(player, (rs, t) -> {
			if(t != null) {
				t.printStackTrace();
			}else {
				if(rs != null) {
//					System.out.println("ClassementPvP : " + player.getName() + " a été ajouté à la bdd.");
				}
			}
		});
		getPlayerPoints(player, (rs, t) -> {
			if(t == null) {
				try {
					if(rs.next()) {
						if(!Main.getInstance().playersMap.containsKey(player.getUniqueId())) {
							Main.getInstance().playersMap.put(player.getUniqueId(), rs.getInt("points"));
							System.out.println("ClassementPvP : " + player.getName() + " a été ajouté au cache");
						}
					}
				} catch (final SQLException e) {
					e.printStackTrace();
				}
			}else {
				t.printStackTrace();
			}
		});
	}  
}