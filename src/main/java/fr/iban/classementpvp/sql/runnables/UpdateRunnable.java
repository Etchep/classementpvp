package fr.iban.classementpvp.sql.runnables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.scheduler.BukkitRunnable;

import fr.iban.classementpvp.sql.Callback;

public class UpdateRunnable extends BukkitRunnable {

    private final Connection connection;
    private final String statement;
    private final Callback<Integer, SQLException> callback;

    public UpdateRunnable(Connection connection, String statement, Callback<Integer, SQLException> callback) {
        this.connection = connection;
        this.statement = statement;
        this.callback = callback;
    }

    @Override
    public void run() {
        final Connection connection = this.connection;
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(statement);

            if(callback != null) {
                callback.call(preparedStatement.executeUpdate(), null);
            }
        } catch (final SQLException e) {
            if(callback != null) {
                callback.call(null, e);
            }
        } finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }

            if(connection != null) {
                try {
                    connection.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
