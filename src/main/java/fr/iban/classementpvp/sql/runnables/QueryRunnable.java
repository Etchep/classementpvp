package fr.iban.classementpvp.sql.runnables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.scheduler.BukkitRunnable;

import fr.iban.classementpvp.sql.Callback;


public class QueryRunnable extends BukkitRunnable {

    private final Connection connection;
    private final String statement;
    private final Callback<ResultSet, SQLException> callback;

    public QueryRunnable(Connection connection, String statement, Callback<ResultSet, SQLException> callback) {
        this.connection = connection;
        this.statement = statement;
        this.callback = callback;
    }

    @Override
    public void run() {
        final Connection connection = this.connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(statement);
            resultSet = preparedStatement.executeQuery();
            if(callback != null) {
                callback.call(resultSet, null);
            }
        } catch (final SQLException e) {
            if(callback != null) {
                callback.call(null, e);
            }
        } finally {
            if(resultSet != null) {
                try {
                    resultSet.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }

            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }

            if(connection != null) {
                try {
                    connection.close();
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
