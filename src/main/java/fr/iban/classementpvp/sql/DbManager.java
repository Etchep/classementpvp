package fr.iban.classementpvp.sql;

import fr.iban.classementpvp.Main;

public enum DbManager {

	BD(new DbCredentials(Main.getInstance().getConfig().getString("database.host"),
			Main.getInstance().getConfig().getString("database.user"),
			Main.getInstance().getConfig().getString("database.pass"),
			Main.getInstance().getConfig().getString("database.dbName"), 3306));
	
	private DbAccess dbAccess;
	
	DbManager(DbCredentials credentials){
		this.dbAccess = new DbAccess(credentials);
	}
	
	public DbAccess getDbAccess(){
		return dbAccess;
	}
	
	
	public static void initAllDbConnections(){
		for(final DbManager dbManager : values()){
			dbManager.dbAccess.initPool();
		}
	}
	
	public static void closeAllDbConnections(){
		for(final DbManager dbManager : values()){
			dbManager.dbAccess.closePool();
		}
	}
}
