package fr.iban.classementpvp.data;

import java.util.UUID;

import lombok.Getter;

public class TopTenData {
	
	@Getter private final int points;
	@Getter private final UUID uuid;
	
	public TopTenData(int points, UUID uuid) {
		this.points = points;
		this.uuid = uuid;
	}

}
