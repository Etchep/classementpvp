package fr.iban.classementpvp.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import fr.iban.classementpvp.sql.SQLCalls;

public class UpdateTask extends BukkitRunnable {

	@Override
	public void run() {
		SQLCalls.updateAllPlayersPoints();
		SQLCalls.updateTopTen();
	}

}
