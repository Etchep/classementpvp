package fr.iban.classementpvp.placeholders;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.iban.classementpvp.Main;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class RegisterPlaceHolders  extends PlaceholderExpansion {

    private final Main plugin;

    /**
     * Since we register the expansion inside our own plugin, we
     * can simply use this method here to get an instance of our
     * plugin.
     *
     * @param plugin
     *        The instance of our plugin.
     */
    public RegisterPlaceHolders(Main plugin){
        this.plugin = plugin;
    }

    /**
     * Because this is an internal class,
     * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
     * PlaceholderAPI is reloaded
     *
     * @return true to persist through reloads
     */
    @Override
    public boolean persist(){
        return true;
    }

    /**
     * Because this is a internal class, this check is not needed
     * and we can simply return {@code true}
     *
     * @return Always true since it's an internal class.
     */
    @Override
    public boolean canRegister(){
        return true;
    }

    /**
     * The name of the person who created this expansion should go here.
     * <br>For convienience do we return the author from the plugin.yml
     * 
     * @return The name of the author as a String.
     */
    @Override
    public String getAuthor(){
        return plugin.getDescription().getAuthors().toString();
    }

    /**
     * The placeholder identifier should go here.
     * <br>This is what tells PlaceholderAPI to call our onRequest 
     * method to obtain a value if a placeholder starts with our 
     * identifier.
     * <br>This must be unique and can not contain % or _
     *
     * @return The identifier in {@code %<identifier>_<value>%} as String.
     */
    @Override
    public String getIdentifier(){
        return "ap";
    }

    /**
     * This is the version of the expansion.
     * <br>You don't have to use numbers, since it is set as a String.
     *
     * For convienience do we return the version from the plugin.yml
     *
     * @return The version as a String.
     */
    @Override
    public String getVersion(){
        return plugin.getDescription().getVersion();
    }

    /**
     * This is the method called when a placeholder with our identifier 
     * is found and needs a value.
     * <br>We specify the value identifier in this method.
     * <br>Since version 2.9.1 can you use OfflinePlayers in your requests.
     *
     * @param  player
     *         A {@link org.bukkit.Player Player}.
     * @param  identifier
     *         A String containing the identifier/value.
     *
     * @return possibly-null String of the requested identifier.
     */
    @Override
    public String onPlaceholderRequest(Player player, String identifier){

        if(player == null){
            return "";
        }

        // %classement_pointsjoueur%
        if(identifier.equals("pts")){
        	if(plugin.playersMap.containsKey(player.getUniqueId())) {
                return plugin.playersMap.get(player.getUniqueId()).toString();
        	}else {
        		return "-";
        	}
        }

        // %classement_joueur1%
        if(identifier.equals("joueur1")){
        	return getTopTenString(1);
        }
        
        // %classement_joueur2%
        if(identifier.equals("joueur2")){
        	return getTopTenString(2);
        }
        
        // %classement_joueur3%
        if(identifier.equals("joueur3")){
        	return getTopTenString(3);
        }
        
        // %classement_joueur4%
        if(identifier.equals("joueur4")){
        	return getTopTenString(4);
        }
        
        // %classement_joueur5%
        if(identifier.equals("joueur5")){
        	return getTopTenString(5);
        }
        
        // %classement_joueur6%
        if(identifier.equals("joueur6")){
        	return getTopTenString(6);
        }
        
        // %classement_joueur7%
        if(identifier.equals("joueur7")){
        	return getTopTenString(7);
        }
        
        // %classement_joueur8%
        if(identifier.equals("joueur8")){
        	return getTopTenString(8);
        }
        
        // %classement_joueur9%
        if(identifier.equals("joueur9")){
        	return getTopTenString(9);
        }
        
        // %classement_joueur10%
        if(identifier.equals("joueur10")){
        	return getTopTenString(10);
        }
 
        // We return null if an invalid placeholder (f.e. %someplugin_placeholder3%) 
        // was provided
        return null;
    }
    
    public String getTopTenString(int position) {
    	if(plugin.top10.containsKey(position)) {
            return Main.getInstance().getConfig().getString("placeholders.top10.format").replaceAll("&", "§").replace("%pseudo%", Bukkit.getOfflinePlayer(plugin.top10.get(position).getUuid()).getName()).replace("%points%", ""+plugin.top10.get(position).getPoints());
    	}else {
    		return "Aucun";
    	}
    }
}
