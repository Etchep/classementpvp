package fr.iban.classementpvp.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.iban.classementpvp.Main;
import fr.iban.classementpvp.sql.SQLCalls;

public class PlayerQuitListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerQuitEvent e) {
		final Player p = e.getPlayer();
		SQLCalls.updatePoints(p, Main.getInstance().playersMap.get(p.getUniqueId()));
		Main.getInstance().playersMap.remove(p.getUniqueId());
	}

}