package fr.iban.classementpvp.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import fr.iban.classementpvp.Main;

public class PlayerDeathListener implements Listener {
	
	
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		final Player killer = e.getEntity().getKiller();
		final Player death = e.getEntity();
		final int KillerMinusDeath = Main.getInstance().playersMap.get(killer.getUniqueId()) - Main.getInstance().playersMap.get(death.getUniqueId());
		final int DeathMinusKiller = Main.getInstance().playersMap.get(killer.getUniqueId()) - Main.getInstance().playersMap.get(death.getUniqueId());
		final int earning = calculateEarnings(KillerMinusDeath);
		final int loss = calculateLoss(DeathMinusKiller);
		if(getPoints(death) >= loss) {
			setPoints(death, getPoints(death) - loss);
			killer.sendMessage("§cVous avez perdu " + loss + " points !");
		}else {
			setPoints(death, 0);
			killer.sendMessage("§cVous n'avez plus de points !");
		}
		setPoints(killer, getPoints(killer) + earning);
		killer.sendMessage("§aVous avez gagné " + earning + " points !");
	}
	
	private int calculateEarnings(int KillerMinusDeath){
		final double i = -0.63599+(60.07066)/(Math.pow(1+0.991798*10, 0.00576*KillerMinusDeath));
		return (int) i;
	}
	
	private int calculateLoss(int DeathMinusKiller){
		final double i = 39.0907-(39.0619)/(Math.pow(1+0.993*10, 0.00595*DeathMinusKiller));
		return (int) i;
	}
	
	private int getPoints(Player p) {
		return Main.getInstance().playersMap.get(p.getUniqueId());
	}
	
	private void setPoints(Player p, int amount) {
		Main.getInstance().playersMap.put(p.getUniqueId(), amount);
	}

}
