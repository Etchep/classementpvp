package fr.iban.classementpvp;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.iban.classementpvp.commands.ApCommand;
import fr.iban.classementpvp.data.TopTenData;
import fr.iban.classementpvp.listeners.PlayerDeathListener;
import fr.iban.classementpvp.listeners.PlayerJoinListener;
import fr.iban.classementpvp.listeners.PlayerQuitListener;
import fr.iban.classementpvp.placeholders.RegisterPlaceHolders;
import fr.iban.classementpvp.sql.DbManager;
import fr.iban.classementpvp.sql.SQLCalls;
import fr.iban.classementpvp.tasks.UpdateTask;
import lombok.Getter;

public class Main extends JavaPlugin {

	@Getter public static Main instance;

	public HashMap<UUID, Integer> playersMap; 
	public ConcurrentHashMap<Integer, TopTenData> top10;
	
	@Override
	public void onEnable() {
		instance = this;
		this.saveDefaultConfig();
		final PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PlayerJoinListener(), this);
		pm.registerEvents(new PlayerQuitListener(), this);
		pm.registerEvents(new PlayerDeathListener(), this);
		getCommand("ap").setExecutor(new ApCommand());
		playersMap = new HashMap<UUID, Integer>();
		top10 = new ConcurrentHashMap<Integer, TopTenData>();
		new UpdateTask().runTaskTimer(this, 0L, 600L);
		if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null){
			new RegisterPlaceHolders(this).register();
		}
		DbManager.initAllDbConnections();
		SQLCalls.createTables();
	}

	@Override
	public void onDisable() {
		DbManager.closeAllDbConnections();
	}
}
